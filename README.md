# Backend "Delilah Restó"

Sprint Project #4 del curso de Desarrollo Web Back End de Acámica.

##  Proyecto desplegado en AWS  🚀

- Aplicacion corriendo en AWS EC2 con Docker.
- Docker file y docker-compose configurado para correr en cualquier servidor.
- IDPS - Google , Facebook , Linkedin , Microsoft Acc.
- Pasarelas de pago - MercadoPago , Paypal. 
- Auth0 y Passport para manejar los usuarios.
- Certificado HTTPS https://www.delilahproject.tk
- Mongo DB Atlas
- AWS Redis ( capa de Cache en Productos )
 

## Listo para usar! 🔧

- Ingrasar al siguiente [Link](https://www.delilahproject.tk/) y loguearse con alguna Idp.
- En el menu desplegable vas a encontrar la seccion Productos (copiar su ID para realizar el pedido)
- Entrar a Swagger para hacer un pedido.
- Una vez realizado , en el menu Mis Pedidos saldra la orden , realizar el pago con MP o Paypal.
- Si el pago es correcto se va actualizar el estado de la orden a Pagada.
- Si ingresa con las credenciales de administrador , se va a desplegar el menu Administrador para agregar/editar/eliminar los Productos.

### Administrador : 

User : admin@delilahproject.tk

Password : admin










