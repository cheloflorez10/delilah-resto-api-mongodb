const  Orders  = require("../models/orders.model");
const  Product  = require("../models/product.model");

const validateOrdenUpdate = async (req, res, next) => {
  const { id } = req.params;
  const { productId, new_amount } = req.body;
  try {
    const index = await Orders.find({ _id : id });
    if (index != 0 ) {
      if (token_info.username === index[0].username) {
        if (index[0].estado === "Pendiente") {
          const index2 = index[0].ped2.findIndex((el) => el._id == productId);
          if (index2 !== -1) {
            if (new_amount) next();
            else {
              res.status(400).json({msg :"No puede dejar este campo en blanco"});
            }
          } else {
            res.status(400).json({msg :"El producto no existe en su orden"});
          }
        } else {
          res.status(400).json({msg : `Error : la orden se encuentra ${index[0].estado}`});
        }
      } else {
        res.status(400).json({msg :"La orden seleccionada no le corresponde"});
      }
    } else {
      res.status(400).json({msg :"Orden no encontrada"});
    }
  } catch (error) {
    res.status(500).json({ msg : "Error en la aplicacion"});
  }
};

const validateStatusUpdate = async (req, res, next) => {
  const { id } = req.params;
  const estado = req.body.estado
  try {
    const index = await Orders.find({ _id : id });
    if (index != 0) {
      if (index[0].estado !== "Pendiente") {
        if (estado == "Preparando" || estado =="Enviado" || estado =="Entregado") 
        next();
        else { res.status(400).json({msg :"Ingrese un estado valido , Preparando - Enviado - Entregado "})}
      } else {
        res.status(400).json({msg :"La orden no esta Confirmada por el usuario"});
      }
    } else {
      res.status(400).json({msg :"Orden no encontrada"});
    }
  } catch (error) {
    res.status(500).json({ msg : "Error en la aplicacion"});
  }
};

const validateDelete = async (req, res, next) => {
  const { id } = req.params;
  const { productId } = req.body;
  try {
    const index = await Orders.find({ _id : id });
    if (index != 0) {
      if (token_info.username === index[0].username) {
        if (index[0].estado === "Pendiente") {
          const index2 = index[0].ped2.findIndex((el) => el._id == productId);
            if (index2 !== -1) next();
            else { res.status(400).json({msg :"El producto no existe en su orden"}); }
        } else {
          res.status(400).json({msg : `Error : la orden se encuentra ${index[0].estado}`});
        }
      } else {
        res.status(400).json({msg :"La orden seleccionada no le corresponde"});
      }
    } else {
      res.status(400).json({msg :"Orden no encontrada"});
    }
  } catch (error) {
    res.status(500).json({ msg : "Error en la aplicacion"});
  }
};

const validateAdd = async (req, res, next) => {
  const { id } = req.params;
  const { productId , amount } = req.body;
  try {
    const index = await Orders.find({ _id : id });
    if (index != 0) {
      if (token_info.username === index[0].username) {
        if (index[0].estado === "Pendiente") {
          const index2 = index[0].ped2.findIndex((el) => el._id == productId);
          const index3 = await Product.find({_id : productId});
          if (index2 === -1 ) {
              if (index3 != 0) {
                if (amount) next();
                  else { 
                    res.status(400).json({msg :"No puede dejar este campo en blanco"});
                  }
                }  else { 
                  res.status(400).json({msg :"El producto no existe"}); 
                }
  
          } else {
            res.status(400).json({msg :"El producto ya existe en su orden , por favor edite la cantidad"});
          }
        } else {
          res.status(400).json({msg : `Error : la orden se encuentra ${index[0].estado}`});
        }
      } else {
        res.status(400).json({msg :"La orden seleccionada no le corresponde"});
      }
    } else {
      res.status(400).json({msg :"Orden no encontrada"});
    }
  } catch (error) {
    res.status(500).json({ msg : "Error en la aplicacion"});
  }
};

const validateCreateOrder = async (req, res, next) => {
  const product = req.body.detail;
  try {
    let productOK = true;
  for (var i = 0, max = product.length; i < max; i += 1) {
    const filtro = await Product.findOne({ _id: product[i].productId });
    if (!filtro) {
      productOK = false;
      break;
    }
  }
  const IDs = new Set(product.map(item => item.productId))
  if ( product.length !== 0) {       
      if (productOK === true) {
        if ([...IDs].length === product.length ) next ();
        else { 
          res.status(400).json({msg :"Producto duplicado"})
        }
      }
      else {
        res.status(400).json({msg :"Un producto seleccionado no se encuntra disponible"});
      }    
  } else {
    res.status(400).json({msg :"Campos en blanco"});
  }
  } catch (error) {
    res.status(500).json({ msg : "Error en la aplicacion"});
  }  
};

const validateClose = async (req,res,next) => {
  const { id } = req.params;
  try {
    const index = await Orders.find({ _id : id });
    if (index != 0) {
      if (token_info.username === index[0].username) {
        if (index[0].estado === "Pendiente") {
          if (index[0].total !== 0) next();
          else {res.status(400).json({msg :"Error : Por favor agregue productos a su orden"}); }
        }
         else {
          res.status(400).json({msg : `Error : la orden se encuentra ${index[0].estado}`});
        }
      } else {
        res.status(400).json({msg :"La orden seleccionada no le corresponde"});
      }
    } else {
      res.status(400).json({msg :"Orden no encontrada"});
    }
  } catch (error) {
    res.status(500).json({ msg : "Error en la aplicacion"});
  }
};

const validateNewaddress = async (req,res,next) => {
  const { id } = req.params;
  const { new_address } = req.body;
  try {
    const index = await Orders.find({ _id : id });
    if (index != 0) {
      if (token_info.username === index[0].username) {
        if (index[0].estado === "Pendiente") {              
            if (new_address) next();
            else {
              res.status(400).json({msg : "No puede dejar este campo en blanco"});
            }       
        } else {
          res.status(400).json({msg : ` Error : la orden se encuentra ${index[0].estado}`});
        }
      } else {
        res.status(400).json({msg :"La orden seleccionada no le corresponde"});
      }
    } else {
      res.status(400).json({msg :"Orden no encontrada"});
    }
  } catch (error) {
    res.status(500).json({ msg : "Error en la aplicacion"});
  }
};

module.exports = {
  validateOrdenUpdate,
  validateStatusUpdate,
  validateDelete,
  validateCreateOrder,
  validateAdd,
  validateClose,
  validateNewaddress
};
