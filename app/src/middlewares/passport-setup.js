const passport = require('passport');
const config = require('../config');
const Auth0Strategy = require('passport-auth0')
const User = require('../models/user.model');


passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});

passport.use(new Auth0Strategy({
  domain: config.auth0_domain,
  clientID: config.auth0_domain_id,
  clientSecret: config.auth0_client_secret,
  callbackURL: config.auth_callback
},(accessToken, refreshToken, extraParams, profile, done) => {
        User.findOne({username: profile.emails[0].value}).then((currentUser) => {
            if(currentUser){                  
                done(null, currentUser );
            } else {
                new User({
                    username: profile.emails[0].value
                }).save().then((newUser) => {                  
                    done(null, newUser);
                });
            }
        });
    })
  );



