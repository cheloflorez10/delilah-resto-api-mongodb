const jwt = require("jsonwebtoken");

function generateToken(user) {
  const token = jwt.sign({ user }, 'delilahsecret' , { expiresIn: "1h" });
  return token;
}

async function validateToken(req, res, next) {
    try {   
    const token = req.cookies.token
    const validation = jwt.verify(token, 'delilahsecret');
    if (validation.user.is_disabled == true) {
      res.status(401).json({msg : "Cuenta deshabilitada"} );
    } else {
      token_info = validation.user;
      next();
    }
  } catch (err) {
    res.status(401).json({msg : "Token invalido o expirado"} );
  }
}

async function validateTokenAdmin(req, res, next) {
    try {
    const token = req.cookies.token
    const validation = jwt.verify(token, 'delilahsecret');
    if (validation.user.is_admin == false) {
      res.status(401).json( {msg : "No tienes permiso de administrador"} );
    } else {
      token_info = validation.user;
      next();
    }
  } catch (err) {
    res.status(401).json({msg : "Token invalido o expirado"} );
  }
}


module.exports = { generateToken, validateToken , validateTokenAdmin};
