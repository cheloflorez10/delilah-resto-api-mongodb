const Product = require("../models/product.model");

const validateProductData = async (req , res , next) => {
  const { nombre , precio } = req.body;
  try {    
    const nombreExistente = await Product.findOne({ nombre: nombre });
    if (nombre && precio) {
      if (!nombreExistente) next();
      else {
        res.status(400).json({msg : "Nombre existente"});
      }
    } else {
      res.status(400).json({msg :"Campos en blanco"});
    }
  } catch (error) {
    res.status(400).json({msg : "Error en la aplicacion"});
  }

};

const validateProductUpdate = async (req, res, next) => {
const { precio } = req.body;
const { id_producto } = req.params;
try {
  const idExistente = await Product.findOne({ _id: id_producto });
  if (precio) {
    if (idExistente) next();
     else {
      res.status(400).json({msg : "Producto no encotrado"} );
    }
  } else {
    res.status(400).json({msg : "Todos los campos deben estar completos" });
  }
} catch (error) {
  res.status(400).json({msg :"Error en la aplicacion"});
}
};

const validatedeleteProduct = async (req, res, next) => {
  try {
    const idExistente = await Product.findOne({ _id: req.params.id });
    if (idExistente) next();
    else {
      res.status(400).json("Producto no existe");
    }
  } catch (error) {
    res.status(400).json("Error en la aplicacion");
  }
};

module.exports = {
  validateProductData,
  validateProductUpdate,
  validatedeleteProduct,
};
