const dotenv = require('dotenv')
dotenv.config()
module.exports = {
    host_page: process.env.HOST_PAGE,
    port: process.env.PORT,
    jwtSecret: process.env.TOKEN_SECRET,
    database: process.env.DATABASE,
    redis_host: process.env.REDIS_HOST,
    redis_port: process.env.REDIS_PORT,
    session_cookieKey: process.env.COOKIEKEY,
    session_key: process.env.SESSIONKEY,
    auth0_domain_id: process.env.AUTH0_CLIENT_ID,
    auth0_domain: process.env.AUTH0_DOMAIN,
    auth0_client_secret: process.env.AUTH0_CLIENT_SECRET,
    auth_callback: process.env.AUTH0_CALLBACK_URL
}

