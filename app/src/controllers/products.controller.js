const Product = require("../models/product.model");
const { clearKey } = require("../DBS/redis");

const createProduct = async (req, res) => {
  const { nombre, precio } = req.body;
  const Producto = new Product({ nombre, precio });
  await Producto.save();
  clearKey(Product.collection.collectionName);
  req.flash("success_msg", "Producto agregado correctamente");
  res.redirect("/productos");
};

const updateProduct = async (req, res) => {
  const { id_producto } = req.params;
  const { precio } = req.body;
  await Product.findByIdAndUpdate(
    { _id: id_producto },
    { precio: precio }
  );
  clearKey(Product.collection.collectionName);
  res.status(200).json({ msg: "Producto actualizado" });
};

const deleteProduct = async (req, res) => {
  await Product.findByIdAndDelete(req.params.id);
  clearKey(Product.collection.collectionName);
  req.flash("success_msg", "Producto Eliminado !");
  res.redirect("/productos");

};

const allProduct = async (req, res) => {
  if (req.user) {
    const Pruducto = await Product.find().cache()
      .sort({ date: "desc" })
      .lean();
    if (req.user.is_admin) {
      res.render("productos/productos-admin", { Pruducto })
    }
    else {
      res.render("productos/productos", { Pruducto })
    }
  }
  else {
    res.redirect('/users/signin')
  }
};

module.exports = { createProduct, allProduct, deleteProduct, updateProduct };

