const Orders = require("../models/orders.model");
const request = require("request")
const config = require('../config');
const mercadopago = require('mercadopago');

// Agrega credenciales
mercadopago.configure({
  access_token: 'TEST-6476947737087118-120715-64be3cb6854cbb2ec810df52f861297e-1034502851'
});

const MercadoPago = async (req, res) => {
  res.cookie('_id', req.body._id)  
  let preference = {
    items: [
      {
        title: 'Orden Delilah Resto',
        unit_price: parseInt(req.body.total),
        quantity: 1,
      }
    ],
    back_urls: {
      "success": `${config.host_page}/pagos/success`,
      "failure": `${config.host_page}/profile/orders`,
      "pending": `${config.host_page}/profile/orders`
    },
    auto_return: "approved",
  };

  mercadopago.preferences.create(preference)
    .then(async function (response) {
      res.redirect(response.body.init_point)
    }).catch(function (error) {
      console.log(error);
    });
}

const CLIENT = 'AZmn4zfz02rzHbnVeQYzIijiLoEKqMM061s7-WM7heGSKiU0-OzA61VHxI4TjtLWLZRH-UbLjMqGZDgb';
const SECRET = 'EDLDuv59kvX_0WDjD4gXm-N6lXCHgvKyByj1A59OMoUHX4pazDpEjEUB2OTJC5NFKleW1x7h8pZJMlnd';
const PAYPAL_API = 'https://api-m.sandbox.paypal.com'; // Live https://api-m.paypal.com
const auth = { user: CLIENT, pass: SECRET }


const Paypal = async (req, res) => {
  res.cookie('_id', req.body._id) 
  const body = {
      intent: 'CAPTURE',
      purchase_units: [{
          amount: {
              currency_code: 'USD', //https://developer.paypal.com/docs/api/reference/currency-codes/
              value: parseInt(req.body.total)
          }
      }],
      application_context: {
          brand_name: `Delilah Project`,
          landing_page: 'NO_PREFERENCE', // Default, para mas informacion https://developer.paypal.com/docs/api/orders/v2/#definition-order_application_context
          user_action: 'PAY_NOW', // Accion para que en paypal muestre el monto del pago
          return_url: `${config.host_page}/pagos/capture-order`, // Url despues de realizar el pago
          cancel_url: `${config.host_page}/pagos/cancel-payment` // Url despues de realizar el pago
      }
  }
  //https://api-m.sandbox.paypal.com/v2/checkout/orders [POST]

  request.post(`https://api-m.sandbox.paypal.com/v2/checkout/orders`, {
      auth,
      body,
      json: true
  }, (err, response) => {
      res.redirect( response.body.links[1].href )
  })
  
};

const capture = async (req, res) => {
  const token = req.query.token; //<-----------

  request.post(`${PAYPAL_API}/v2/checkout/orders/${token}/capture`, {
      auth,
      body: {},
      json: true
  }, (err, response) => {
    res.redirect(`${config.host_page}/pagos/success`)
  })
};

const success = async  (req, res) => { 
  const id  = req.cookies._id;
  await Orders.findByIdAndUpdate(
    { _id: id },
    { estado: "PAGADA"}
  );
  res.redirect(`${config.host_page}/profile/orders`)
};

const cancel = (req, res) => {
  res.redirect(`${config.host_page}/profile/orders`);
};

module.exports = { MercadoPago, Paypal, capture , success,cancel};