const Orders  = require("../models/orders.model");
const User = require("../models/user.model")


const allOrders = async (req,res) => {
    const orders = await Orders.find({})
    res.json(orders)
}
    
const editStatus = async (req , res) => {
  const nuevoEstado = req.body.estado
  const { id } = req.params
  try {
    await Orders.findByIdAndUpdate(
      { _id: id },
      { estado: nuevoEstado}
    );
    res.status(200).json({msg : "Estado del pedido actualizado correctamente !"} );
  } catch (error) {
    res.status(500).json({msg : "Error en la aplicacion"} );
  }

}

const disableUser = async (req , res) => {
  const { id } = req.params
  try {
    await User.findByIdAndUpdate(
      { _id: id },
      { is_disabled: true}
    );
    res.status(200).json({msg : "Usuario desactivado existosamente !"} );
  } catch (error) {
    res.status(500).json({msg : "Error en la aplicacion"} );
  }

}

const activeUser = async (req , res) => {
  const { id } = req.params
  try {    
    await User.findByIdAndUpdate(
      { _id: id },
      { is_disabled: false}
    );
    res.status(200).json({msg : "Usuario activado existosamente !"} );
  } catch (error) {
    res.status(500).json({msg : "Error en la aplicacion"} );
  }

}

module.exports = { allOrders , editStatus , disableUser ,activeUser}