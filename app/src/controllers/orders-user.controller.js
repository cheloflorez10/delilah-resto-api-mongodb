const Orders = require("../models/orders.model");
const Product = require("../models/product.model");


const createOrder = async (req, res) => {
  const { username } = token_info;
  const { address } = req.body;
  const Productos = req.body.detail;
  const estado = "Pendiente";
  const ped2 = [];
  let total = 0;
  for (var i = 0, max = Productos.length; i < max; i += 1) {
    const filtro = await Product.find({ _id: Productos[i].productId });
    total = total + filtro[0].precio * Productos[i].product_amount;
    var ped = {
      _id: filtro[0]._id,
      Nombre: filtro[0].nombre,
      Cantidad: Productos[i].product_amount,
      "Precio Unitario": filtro[0].precio,
    };
    ped2.push(ped);
  }
  const order = new Orders({
    username,
    address,
    total,
    ped2,
    estado,
  });
  await order.save();
  res.status(200).json({ msg: "Orden creada correctamente , busquela con el siguiente ID :  " + `${order._id}`});
};

const historyOrders = async (req, res) => {
  try {
    const result = await Orders.find({ username: token_info.username }, [
      "address",
      "ped2",
      "total",
      "estado",
    ]);

    if (result.length == 0) res.status(400).json({ msg: "No hay Ordenes" });
    else {
      res.status(200).json(result);
    }
  } catch (error) {
    res.status(500).json({ msg: "Error en la aplicacion" });
  }
};

const editOrder = async (req, res) => {
  let suma = 0;
  const { id } = req.params;
  const { productId, new_amount } = req.body;
    const index = await Orders.findOne({ _id: id });
    const index2 = index.ped2.findIndex((el) => el._id == productId);
    index.ped2[index2].Cantidad = new_amount;
    for (var i = 0, max = index.ped2.length; i < max; i += 1) {
      const filtro = await Product.find({ _id: index.ped2[i]._id });
      suma = suma += filtro[0].precio * index.ped2[i].Cantidad;
    }
    index.total = suma;
    index.markModified("ped2");
    await index.save();
    res.status(200).json({ msg : "Cantidad del producto actualizado , orden Pendiente de confirmacion"} );
};

const deleteOrderProduct = async (req, res) => {
  const { id } = req.params;
  const { productId } = req.body;
    let suma = 0;
    const index = await Orders.findOne({ _id: id });
    const index2 = index.ped2.findIndex((el) => el._id == productId);
    index.ped2.splice(index2, 1);
    for (var i = 0, max = index.ped2.length; i < max; i += 1) {
      const filtro = await Product.find({ _id: index.ped2[i]._id });
      suma = suma += filtro[0].precio * index.ped2[i].Cantidad;
    }
    index.total = suma;
    await index.save();
    res.status(200).json({msg : "Producto eliminado y orden Pendiente de confirmacion"});
};

const addOrderProduct = async (req, res) => {
  const { id } = req.params;
  const { productId, amount } = req.body;
  let suma = 0;
  const index = await Orders.findOne({ _id: id });
  const filtro = await Product.findOne({ _id: productId });
  var ped = {
    _id: filtro._id,
    Nombre: filtro.nombre,
    Cantidad: amount,
    "Precio Unitario": filtro.precio,
  };
  index.ped2.push(ped);

  for (var i = 0, max = index.ped2.length; i < max; i += 1) {
    const filtro = await Product.find({ _id: index.ped2[i]._id });
    suma = suma += filtro[0].precio * index.ped2[i].Cantidad;
  }

  index.total = suma;
  await index.save();
  res.status(200).json({ msg : "Producto agregado , orden Pendiente de confirmacion"});
};

const closeOrder = async (req, res) => {
  const { id } = req.params;
  await Orders.findByIdAndUpdate(
    { _id: id },
    { estado: "Cerrado"}
  );
  res.status(200).json({ msg : "Orden Cerrada"});
};

const addAdress = async (req, res) => {
  const { id } = req.params;
  const { new_address } = req.body;
  await Orders.findByIdAndUpdate(
    { _id: id },
    { address: new_address}
  );
  res.status(200).json({ msg : "Direccion de envio actualizada"});
};

module.exports = {
  createOrder,
  historyOrders,
  editOrder,
  deleteOrderProduct,
  addOrderProduct,
  closeOrder,
  addAdress,
};
