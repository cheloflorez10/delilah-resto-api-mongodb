// Express
const express = require("express");
const router = express.Router()

// JWT Auth.

const {validateTokenAdmin}  = require("../middlewares/jwt.middleware")

// Controllers
const { allOrders , editStatus , disableUser , activeUser} = require("../controllers/orders-admin.controller");

// Middlewares
const {validateStatusUpdate} = require("../middlewares/orders.middlewares");

// Routes

router.get("/allorders", validateTokenAdmin , allOrders ) // VER TODAS LAS ORDENES

router.put("/:id", validateTokenAdmin , validateStatusUpdate, editStatus); // EDITAR ESTADO DE ORDEN

router.post("/disabled/:id", validateTokenAdmin , disableUser ); // DESACTIVAR UN USUARIO

router.post("/active/:id", validateTokenAdmin , activeUser ); // ACTIVAR UN USUARIO

module.exports = router

