// Express
const express = require("express");
const router = express.Router()

// JWT Auth.

const {validateToken} = require("../middlewares/jwt.middleware")

// Controller
const { createOrder , historyOrders , editOrder , deleteOrderProduct , addOrderProduct, closeOrder, addAdress } = require("../controllers/orders-user.controller")

// Middlewares
const {validateOrdenUpdate , validateDelete, validateCreateOrder, validateAdd , validateClose , validateNewaddress} = require("../middlewares/orders.middlewares")

// Routes


router.post("/new", validateToken,validateCreateOrder ,createOrder); // CREAR UNA ORDEN

router.get("/history", validateToken , historyOrders ) // VER HISTORIAL DE ORDENES

router.put("/edit/:id" , validateToken , validateOrdenUpdate , editOrder) // EDITAR PRODUCTO DE UNA ORDEN

router.delete("/delete/:id", validateToken , validateDelete , deleteOrderProduct) // ELIMINAR PRODUCTO DE UNA ORDEN

router.post("/add/:id",  validateToken , validateAdd , addOrderProduct) // AGREGAR UN PRODUCTO A UNA ORDEN

router.put("/newaddress/:id", validateToken,  validateNewaddress, addAdress ) // AGREGAR UNA DIRECCION DE ENVIO

router.post("/close/:id",validateToken,validateClose, closeOrder) // CERRAR UNA ORDEN


module.exports = router

