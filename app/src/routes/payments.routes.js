// Express
const express = require("express");
const router = express.Router()

// Controllers
const { MercadoPago,Paypal, capture, success, cancel} = require("../controllers/payments.controller");

router.post("/pagar", MercadoPago ); // 

router.post("/paypal",Paypal);

router.get("/capture-order", capture); // ELIMINAR UN PRODUCTO

router.get("/success", success); // EDITAR UN PRODUCTO

router.get("/cancel-payment", cancel); // EDITAR UN PRODUCTO


module.exports = router

