const router = require('express').Router();
const path = require('path')

router.get('/', (req, res) => {
    res.render("index");;
});

router.get("/swagger",  (req, res) => {
    if (req.cookies.token)
    res.sendFile(path.resolve('src/views/swagger/index.html'))
    else res.redirect("/users/signin");
  });


module.exports = router;