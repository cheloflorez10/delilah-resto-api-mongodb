// Express
const express = require("express");
const router = express.Router()

// JWT Auth.
const {validateTokenAdmin , validateToken }  = require("../middlewares/jwt.middleware")

// Controllers
const { allProduct,createProduct, updateProduct, deleteProduct} = require("../controllers/products.controller");


// Middlewares
const {validateProductData, validateProductUpdate,validatedeleteProduct } = require("../middlewares/products.middlewares");


// Routes

router.get("/", validateToken , allProduct); // VER TODOS LOS PRODUCTOS

router.post("/agregar",validateTokenAdmin , validateProductData , createProduct );

router.delete("/delete/:id", validateTokenAdmin, validatedeleteProduct, deleteProduct); // ELIMINAR UN PRODUCTO

router.put("/edit/:id_producto", validateTokenAdmin, validateProductUpdate , updateProduct); // EDITAR UN PRODUCTO

module.exports = router
