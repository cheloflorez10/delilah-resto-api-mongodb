const express = require("express");
const router = express.Router()

// use all the routes

router.use('/profile', require('./profile.routes'))
router.use('/', require('./index.routes'))
router.use('/users', require('./user.routes'))
router.use('/productos', require('./product.routes'))
router.use('/pagos', require('./payments.routes'))
router.use('/admin', require('./admin.routes'))
router.use('/orders/user', require('./orders-user.routes'))

module.exports = router