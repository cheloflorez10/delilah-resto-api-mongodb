const router = require('express').Router();
const passport = require('passport');
const {generateToken} = require('../middlewares/jwt.middleware');
const url = require('url')
const querystring = require('querystring')
const util = require('util')
const config = require('../config');

router.get('/signin', passport.authenticate('auth0', {
    scope: 'openid email profile'
  }), function (req, res) {
    res.redirect('/');
  });


router.get('/logout', (req, res) => {
    res.clearCookie('token',{path:'/'});
    req.logout();
    var returnTo = `${config.host_page}`;
    // var port = req.connection.localPort;
    // if (port !== undefined && port !== 80 && port !== 443) {
    //   returnTo += ':' + port;
    // }
    var logoutURL = new url.URL(
      util.format('https://%s/v2/logout', process.env.AUTH0_DOMAIN)
    );
    var searchString = querystring.stringify({
      client_id: process.env.AUTH0_CLIENT_ID,
      returnTo: returnTo
    });
    logoutURL.search = searchString;
    req.flash("success_msg", "You are logged out now.");
    res.redirect(logoutURL);
  });

  router.get('/callback', passport.authenticate('auth0'), (req, res) => {
    if (req.user) {
      const token = generateToken(req.user)
      res.cookie('token', token)    
      res.redirect('/profile')
    } else {
      res.redirect('/users/signin')
    }
  }
);

module.exports = router;
