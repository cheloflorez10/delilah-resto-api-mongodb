const router = require('express').Router();
const Orders = require('../models/orders.model')

router.get("/", async (req, res) => {
  if (req.user) {
    const user = req.user
    res.render("profile/profile", { user })
  }
  else {
    res.redirect('/users/signin')
  }
});

router.get("/orders", async (req, res) => {
  res.clearCookie('_id',{path:'/'});
  if (req.user) {
  const orders = await Orders.find({ username: req.user.username })
    .sort({ date: "desc" })
    .lean();
  res.render("orders/orders", { orders })}
  else {
    res.redirect('/users/signin')
  }
});


module.exports = router;