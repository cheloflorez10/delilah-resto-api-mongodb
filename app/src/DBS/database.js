const mongoose = require("mongoose");
const config = require("../config");

if (process.env.NODE_ENV === 'test') {
  const Mockgoose = require('mockgoose').Mockgoose;
  const mockgoose = new Mockgoose(mongoose);
  mockgoose.prepareStorage().then(() => {
    mongoose
    .connect(config.database, { useNewUrlParser: true, useUnifiedTopology: true })
    .then((db) => console.log("✅ 💃 DB TEST connected !"))
    .catch((err) => console.log(err))
  })
 } else {mongoose
  .connect(config.database, { useNewUrlParser: true, useUnifiedTopology: true })
  .then((db) => console.log("✅ 💃 DB connected !"))
  .catch((err) => console.log(err))
 }

module.exports = {mongoose} ;
