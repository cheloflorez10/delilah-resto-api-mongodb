const mongoose = require("mongoose");
const redis = require("redis");
const util = require("util");
const config = require("../config")

const client = redis.createClient({
  host: config.redis_host,
  port: config.redis_port,
});

client.on("connect", () => {
    console.log('✅ 💃 Redis connected !')
});

client.on("error", (error) => {
    console.log(`❗️ Redis Error: failed to connect ${config.redis_host} to port : ${config.redis_port}`)
    process.exit();
});

client.hget = util.promisify(client.hget);

const exec = mongoose.Query.prototype.exec;

mongoose.Query.prototype.cache = function(options = { time: 60 }) {
  this.useCache = true;
  this.time = options.time;
  this.hashKey = JSON.stringify(options.key || this.mongooseCollection.name);

  return this;
};

mongoose.Query.prototype.exec = async function() {
  if (!this.useCache) {
    return await exec.apply(this, arguments);
  }

  const key = JSON.stringify({
    ...this.getQuery()
  });

  const cacheValue = await client.hget(this.hashKey, key);

  if (cacheValue) {
    const doc = JSON.parse(cacheValue);

    console.log(`Response from Cache in : ${this.mongooseCollection.name}`);
    return Array.isArray(doc)
      ? doc.map(d => new this.model(d))
      : new this.model(doc);
  }

  const result = await exec.apply(this, arguments);
  client.hset(this.hashKey, key, JSON.stringify(result));
  client.expire(this.hashKey, this.time);

  console.log(`Response from MongoDB in : ${this.mongooseCollection.name}`);
  return result;
};

module.exports = {
  clearKey(hashKey) {
    client.del(JSON.stringify(hashKey));
  }
};
