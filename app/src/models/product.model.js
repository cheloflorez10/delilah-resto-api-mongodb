const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = Schema({
  nombre: String,
  precio: Number ,
},{
  versionKey: false,
  timestamps: true,
});


module.exports = mongoose.model('Product', ProductSchema);
