const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderSchema = Schema(
  {
    username: String,
    fullname: String,
    address: String,
    ped2: Array,
    total: Number,
    method_of_payment: String,
    estado: String,
  },
  {
    timestamps: true,
    versionKey: false,
  }
);

module.exports = mongoose.model("Orders", orderSchema);