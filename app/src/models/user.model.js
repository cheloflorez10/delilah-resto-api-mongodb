const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = Schema(
  { 
    username: String,
    is_admin: { type: Boolean, default: false },
    is_disabled: { type: Boolean, default: false }
  },
  {
    timestamps: true,
    versionKey: false,
  }
);


module.exports = mongoose.model('User', userSchema);
