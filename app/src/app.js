// Express
const express = require('express');
const app = express();
const helmet = require('helmet')
const bodyParser = require('body-parser')
const methodOverride = require('method-override');
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(methodOverride('_method'));

app.use(
  helmet({
    contentSecurityPolicy: false,
  })
);

// Configs
const config = require('./config');

// Cookie & Sessions
const cookieSession = require('cookie-session');
const cookieParser = require('cookie-parser');
const session = require('express-session');
app.use(cookieParser())

// set up session cookies
app.use(cookieSession({
  maxAge: 24 * 60 * 60 * 1000,
  keys: [config.session_cookieKey]
}));

app.use(session({
  secret: config.session_key,
  resave: false,
  saveUninitialized: false
}));

// set view engine
const path = require('path')
const exphbs = require('express-handlebars')
const Handlebars = require('handlebars')
const {allowInsecurePrototypeAccess} = require('@handlebars/allow-prototype-access')
const flash =require('connect-flash');
app.use(express.static(path.join(__dirname, "public")));
app.set("views", path.join(__dirname, "views"));
app.engine(
    ".hbs",
    exphbs({
      handlebars: allowInsecurePrototypeAccess(Handlebars),
      defaultLayout: "main",
      layoutsDir: path.join(app.get("views"), "layouts"),
      partialsDir: path.join(app.get("views"), "partials"),
      extname: ".hbs",
      helpers:{equal:function(a,b, options){return (a!=b)?options.fn(this):options.inverse(this)}},
    })
);
app.set("view engine", ".hbs");
app.use(flash());

// initialize passport
const passport = require('passport');
const passportSetup = require('./middlewares/passport-setup');
app.use(passport.initialize());
app.use(passport.session());


// Global Variables
app.use((req, res, next) => {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error = req.flash("error");
  res.locals.user = req.user || null;
  next();
});

// Routes

app.use('/', require('./routes/index'))


// Connection to db

const { mongoose } = require('./DBS/database');

// Server Init
app.listen(config.port, () => {
  console.log(`Delilah Resto - Server Started on port ${config.port}`);
});

