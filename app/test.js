function login(email, password, callback) {
  const bcrypt = require('bcrypt');
  const MongoClient = require('mongodb@3.1.4').MongoClient;
  const client = new MongoClient('mongodb+srv://mongod:'+configuration.MONGO_PASSWORD+'@cluster0.xluul.mongodb.net/Test?retryWrites=true&w=majority');

  client.connect(function (err) {
    if (err) return callback(err);

    const db = client.db('Test');
    const users = db.collection('users');

    users.findOne({ username: email }, function (err, user) {
      if (err || !user) {
        client.close();
        return callback(err || new WrongUsernameOrPasswordError(email));
      }

      bcrypt.compare(password, user.password, function (err, isValid) {
        client.close();

        if (err || !isValid) return callback(err || new WrongUsernameOrPasswordError(email));

        return callback(null, {
            user_id: user._id.toString(),
            displayName: user.username
          });
      });
    });
  });
}

function create(user, callback) {
  const bcrypt = require('bcrypt');
  const MongoClient = require('mongodb@3.1.4').MongoClient;
  const client = new MongoClient('mongodb+srv://mongod:'+configuration.MONGO_PASSWORD+'@cluster0.xluul.mongodb.net/Test?retryWrites=true&w=majority');

  client.connect(function (err) {
    if (err) return callback(err);

    const db = client.db('Test');
    const users = db.collection('users');

    users.findOne({ username: user.email }, function (err, withSameMail) {
      if (err || withSameMail) {
        client.close();
        return callback(err || new Error('the user already exists'));
      }

      bcrypt.hash(user.password, 10, function (err, hash) {
        if (err) {
          client.close();
          return callback(err);
        }

        user.password = hash;

        const user1 = {
          fullname: user.nickname,
          username: user.email,
          password : user.password,
          is_admin: false,
          is_disabled: false,
          provider : user.connection
        };
        users.insert(user1, function (err, inserted) {
          client.close();

          if (err) return callback(err);
          callback(null);
        });
      });
    });
  });
}